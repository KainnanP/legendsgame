<?php
error_reporting(E_ALL);
require 'vendor/autoload.php';

// Using Medoo namespace
use Medoo\Medoo;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'brigada',
    'charset' => 'latin1_swedish_ci',
    'server' => 'wwwjoyz.clasagcj0tjw.us-east-1.rds.amazonaws.com',
    'username' => 'brigada',
    'password' => 'kp6969'
]);


$countCard = $database->count("legends_game", [
	"card_number" => $_POST["card"]
]);

$countEmail = $database->count("legends_game", [
	"cpf" => $_POST["cpf"]
]);
//var_dump($countCard);

if($countCard == 0 &&  $countEmail == 0){
  $database->insert("legends_game", [
  	"name" => utf8_decode($_POST["name"]),
  	"email" => $_POST["email"],
  	"password" => $_POST["password"],
  	"cpf" => $_POST["cpf"],
  	"cause" => $_POST["cause"],
  	"card_number" => $_POST["card"]
  ]);
  $account_id = $database->id();
}


?>
<!DOCTYPE html>
<html>

  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130556198-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-130556198-1');
    </script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PZM27VF');</script>
<!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Legends Game + Joyz</title>
    <meta property="og:title" content="Legends Game + Joyz">
    <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto+Condensed" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>
    <script src="assets/js/creditcard.js" ></script>
    <script src="assets/js/main.js" ></script>
    <link rel="stylesheet" href="assets/css/main.css">
  </head>

  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZM27VF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- <div  class="container-fluid">
      <div class="row">
        <div id="header" class="col-md-12">
          <img id="lelogo"src="assets/images/legends-logo.png" alt="">
          <h1>Bem vindo a plataforma de inscrição do Legends Game</h1>
          <img id="lojoy" src="assets/images/logo-joyz.png" alt="">
        </div>
      </div>
    </div> -->
    <div class="container">
      <div class="row">

          <div class="col-md-12">
          </div>
      </div>
      <?php if($countCard > 0 || $countEmail > 0): ?>
        <div class="row">
          <div id="error" class="text-center overal-result col-md-8 offset-md-2">
            <img src="http://cdn.joyz.me/legendsgame/assets/images/red.png" width="300" alt="">
            <div class="info-box">
              <h2>Opsss Tivemos um problema!</h2>
              <p>O seu CPF já esta cadastrado!</p>
              <a href="javascript:history.back()" id="go">Tentar novamente</a>
            </div>
          </div>
        </div>
      <?php else: ?>
        <div class="row">
          <div id="error" class="text-center overal-result col-md-8 offset-md-2">
            <img src="http://cdn.joyz.me/legendsgame/assets/images/crowd.png" style="width:100%" alt="">
            <div class="info-box">
              <h2>Agora é só aproveitar!!!</h2>
              <a href="http://joyz.me/legendsgame" id="go">Voltar para a home</a>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </body>

</html>
