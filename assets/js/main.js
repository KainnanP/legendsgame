window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');

    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

      form.addEventListener('submit', function(event) {
        console.log(checkCreditCard(document.getElementsById('card').val(), 'visa'));
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });





}, false);

$( document ).ready(function() {
  $('#registerform').submit(function(e){
    $('#card').validateCreditCard(function(result) {
      if((result.card_type != null) && (result.valid == true) && (result.card_type.name == 'visa' || result.card_type.name == 'visa_electron')){
        $(".invalid").hide();
        return
      }else{
        e.preventDefault();
        $(".invalid").show();
        return
      }
    });
    return
  });
});
